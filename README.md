Import a profile
================

The script file `import_profile` should be added on `script`folder.

To run:

```
./script/import_profile <yaml file>
```

Example:

```
./script/import_profile config/example_profile.yml
```
